package facci.pm.moshi;

import java.util.List;

class BlackjackHand {
    public final Card hidden_card;
    public final List<Card> visible_cards;

    public BlackjackHand(Card hidden_card, List<Card> visible_cards) {
        this.hidden_card = hidden_card;
        this.visible_cards = visible_cards;
    }
}

class Card {
    public final char rank;
    public final Suit suit;

    public Card(char rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }
}

enum Suit {
    CLUBS, DIAMONDS, HEARTS, SPADES;
}